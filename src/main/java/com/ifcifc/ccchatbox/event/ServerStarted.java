package com.ifcifc.ccchatbox.event;
import com.ifcifc.ccchatbox.CCChatBox;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents.ServerStarting;
import net.minecraft.server.MinecraftServer;

public class ServerStarted implements ServerStarting{
    @Override
    public void onServerStarting(MinecraftServer server) {
        CCChatBox.INSTANCE.setMinecraftServer(server);
    }
}
