package com.ifcifc.ccchatbox.event;

import com.ifcifc.ccchatbox.CCChatBox;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.minecraft.server.MinecraftServer;

public class ServerStopped implements ServerLifecycleEvents.ServerStarted {
    @Override
    public void onServerStarted(MinecraftServer server) {
        CCChatBox.INSTANCE.setMinecraftServer(null);
    }
}
