package com.ifcifc.ccchatbox.blockEntity;

import com.ifcifc.ccchatbox.data.CCCRegistries;
import com.ifcifc.ccchatbox.peripherals.ChatBlockPeripheral;
import dan200.computercraft.api.peripheral.IPeripheral;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import org.jetbrains.annotations.Nullable;

public class ChatBlockEntity extends BlockEntity implements PeripheralBlockEntity {
    private ChatBlockPeripheral peripheral;

    public ChatBlockEntity(BlockPos pos, BlockState state) {
        super((BlockEntityType<ChatBlockEntity>) CCCRegistries.CHAT_BLOCK_ENTITY.get(), pos, state);
    }

    public IPeripheral getPeripheral(@Nullable Direction side) {
        if (peripheral == null)
            peripheral = new ChatBlockPeripheral(this);
        return peripheral;
    }

}
