package com.ifcifc.ccchatbox.block;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import static com.ifcifc.ccchatbox.data.PropertiesBuilder.PROPERTIES;

public class TweakedBlockItem extends BlockItem {
    private final double version;

    public TweakedBlockItem(Block block, @Nullable double version) {
        super(block, PROPERTIES);
        this.version = version;
    }

    @Environment(EnvType.CLIENT)
    @Override
    public void appendHoverText(ItemStack stack, @Nullable Level level, List<Component> tooltip, TooltipFlag flag) {
        String[] lines = Component.translatable(super.getDescriptionId() + ".description").getString().split("\n");
        for (String line : lines) {
            tooltip.add(Component.literal(line));
        }
    }
}
