package com.ifcifc.ccchatbox.block;

import com.ifcifc.ccchatbox.blockEntity.ChatBlockEntity;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import org.jetbrains.annotations.Nullable;

public class ChatBlock extends Block implements EntityBlock {
    public static final Properties CHAT_BLOCK_PROPERTIES = FabricBlockSettings.create().strength(3.0f).sound(SoundType.METAL);
    public ChatBlock(Properties properties) {
        super(properties);
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return new ChatBlockEntity(pos, state);
    }
}
