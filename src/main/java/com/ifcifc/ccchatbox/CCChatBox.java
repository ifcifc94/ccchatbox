package com.ifcifc.ccchatbox;

import com.ifcifc.ccchatbox.blockEntity.PeripheralBlockEntity;
import com.ifcifc.ccchatbox.data.CCCRegistries;
import com.ifcifc.ccchatbox.event.ServerStarted;
import com.ifcifc.ccchatbox.event.ServerStopped;
import com.ifcifc.ccchatbox.peripherals.ChatBlockPeripheral;
import dan200.computercraft.api.peripheral.PeripheralLookup;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.fabric.api.message.v1.ServerMessageEvents;
import net.minecraft.server.MinecraftServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CCChatBox implements ModInitializer {
	public static final String MOD_ID = "ccchatbox";
	public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);
	private MinecraftServer minecraftServer;
	public static CCChatBox INSTANCE;

	@Override
	public void onInitialize() {
		INSTANCE = this;

		ServerLifecycleEvents.SERVER_STARTED.register(new ServerStarted()::onServerStarting);
		ServerLifecycleEvents.SERVER_STOPPED.register(new ServerStopped()::onServerStarted);

		ServerMessageEvents.CHAT_MESSAGE.register((message, sender, params)->ChatBlockPeripheral.messageEvent(
								message.signedContent(),
								sender.getName().getString(),
								sender.getUUID().toString()));

		// Minecraft stuff
		CCCRegistries.register();

		// ComputerCraft stuff
		PeripheralLookup.get().registerFallback((world, pos, state, blockEntity, direction) -> {
			if (blockEntity instanceof PeripheralBlockEntity peripheralBlockEntity)
				return peripheralBlockEntity.getPeripheral(direction);
			return null;
		});



		LOGGER.info("Server Initialize");
	}

	public void setMinecraftServer(MinecraftServer minecraftServer) {
		this.minecraftServer = minecraftServer;
	}

	public MinecraftServer getMinecraftServer(){
		if(this.minecraftServer==null){
			throw new RuntimeException("getMinecraftServer -> minecraftServer is null");
		}
		return this.minecraftServer;
	}
}