package com.ifcifc.ccchatbox.data;

import com.ifcifc.ccchatbox.CCChatBox;
import com.ifcifc.ccchatbox.block.ChatBlock;
import com.ifcifc.ccchatbox.block.TweakedBlockItem;
import com.ifcifc.ccchatbox.blockEntity.ChatBlockEntity;
import com.ifcifc.ccchatbox.peripherals.ChatBlockPeripheral;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.entity.decoration.PaintingVariant;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;

import java.util.Arrays;
import java.util.List;

import static com.ifcifc.ccchatbox.block.ChatBlock.CHAT_BLOCK_PROPERTIES;

public enum CCCRegistries {
    CHAT_BLOCK("chat_block", new ChatBlock(CHAT_BLOCK_PROPERTIES)),
    CHAT_BLOCK_ITEM(CHAT_BLOCK.entry, ChatBlockPeripheral.getVersion()),

    CHAT_BLOCK_ENTITY("chat_block_entity", BlockEntityType.Builder.of(ChatBlockEntity::new, (ChatBlock)CHAT_BLOCK.get()).build(null)),

    FUNNY_REDROUTERS_PAINTING("funny_redrouters", new PaintingVariant(32,16));

    public static final ResourceKey<CreativeModeTab> CCCGROUP = ResourceKey.create(Registries.CREATIVE_MODE_TAB, new ResourceLocation(CCChatBox.MOD_ID, "main_group"));

    public static void register() {
        Registry.register(BuiltInRegistries.CREATIVE_MODE_TAB, CCCGROUP, PropertiesBuilder.CCCGROUP);

        for (CCCRegistries entry : CCCRegistries.ENTRIES) {
            switch (entry.type()) {
                case BLOCK -> Registry.register(BuiltInRegistries.BLOCK, entry.resourceLocation(), (Block)entry.get());
                case ITEM -> {
                    Item item = (Item)entry.get();
                    Registry.register(BuiltInRegistries.ITEM, entry.resourceLocation(), item);
                    // Add to item group
                    ItemGroupEvents.modifyEntriesEvent(CCCGROUP).register(content -> {
                        content.accept(item);
                    });
                }
                case BLOCK_ENTITY -> Registry.register(BuiltInRegistries.BLOCK_ENTITY_TYPE, entry.resourceLocation(),  (BlockEntityType<? extends BlockEntity>)entry.get());
                case SOUND_EVENT -> Registry.register(BuiltInRegistries.SOUND_EVENT, entry.resourceLocation(), (SoundEvent)entry.get());
                case PAINTING_VARIANT -> Registry.register(BuiltInRegistries.PAINTING_VARIANT, entry.resourceLocation(), (PaintingVariant)entry.get());
            }
        }
    }

    public static final List<CCCRegistries> ENTRIES = Arrays.asList(CCCRegistries.values());

    private final RegisterEntry entry;

    CCCRegistries(String id, Block entry) {
        this.entry = new RegisterEntry(id, entry, RegisterEntry.TYPE.BLOCK);
    }

    <T extends BlockEntity> CCCRegistries(String id, BlockEntityType<T> entry) {
        this.entry = new RegisterEntry(id, entry, RegisterEntry.TYPE.BLOCK_ENTITY);
    }

    CCCRegistries(RegisterEntry block, double version) {
        this.entry = new RegisterEntry(block.id(), new TweakedBlockItem((Block)block.entry(), version), RegisterEntry.TYPE.ITEM);
    }

    CCCRegistries(String id) {
        this.entry = new RegisterEntry(id, SoundEvent.createVariableRangeEvent(new ResourceLocation(CCChatBox.MOD_ID, id)), RegisterEntry.TYPE.SOUND_EVENT);
    }

    CCCRegistries(String id, PaintingVariant entry) {
        this.entry = new RegisterEntry(id, entry, RegisterEntry.TYPE.PAINTING_VARIANT);
    }

    public Object get() {
        return entry.entry();
    }

    public RegisterEntry.TYPE type() {
        return entry.type();
    }

    public String id() {
        return entry.id();
    }

    public ResourceLocation resourceLocation() {
        return new ResourceLocation(CCChatBox.MOD_ID, entry.id());
    }
}
