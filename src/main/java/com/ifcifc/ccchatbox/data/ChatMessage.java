package com.ifcifc.ccchatbox.data;

import java.io.Serializable;

public class ChatMessage implements Serializable {
    public String message;
    public String playerName;
    public String playerUUID;

    public ChatMessage(String message, String playerName, String playerUUID) {
        this.message = message;
        this.playerName = playerName;
        this.playerUUID = playerUUID;
    }
}
