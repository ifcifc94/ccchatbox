package com.ifcifc.ccchatbox.data;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;

public class PropertiesBuilder {
    public static final CreativeModeTab CCCGROUP = FabricItemGroup.builder()
            .title(Component.translatable("itemGroup.ccchatbox.ccchatbox_group"))
            .icon(() -> new ItemStack((Item) CCCRegistries.CHAT_BLOCK_ITEM.get()))
            .build();
    public static final FabricItemSettings PROPERTIES = new FabricItemSettings().maxCount(64);
}