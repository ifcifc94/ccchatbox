package com.ifcifc.ccchatbox.peripherals;

import com.ifcifc.ccchatbox.CCChatBox;
import com.ifcifc.ccchatbox.blockEntity.ChatBlockEntity;
import com.mojang.authlib.GameProfile;
import dan200.computercraft.api.lua.LuaFunction;
import net.fabricmc.fabric.api.entity.FakePlayer;
import net.minecraft.network.chat.ChatType;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.OutgoingChatMessage;
import net.minecraft.server.level.ServerPlayer;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * This peripheral is used by the Source Block. It is used to give some kind of Create Display Targets data.
 * The peripheral acts similar to a normal Terminal, with some implementations from the Window API and other limitations like no control over the colors.
 *
 * @version 1.1
 */
public class ChatBlockPeripheral extends TweakedPeripheral<ChatBlockEntity> {
    //private static final List<IComputerAccess> COMPUTERS = new LinkedList<>();
    private static final List<ChatBlockPeripheral> PERIPHERALS_LIST = new LinkedList<>();
    public static double getVersion() {
        return 1.1D;
    }

    public ChatBlockPeripheral(ChatBlockEntity blockentity) {
        super("create_chat", blockentity);
        PERIPHERALS_LIST.removeIf(P ->
                P.blockEntity.getBlockPos().equals(blockentity.getBlockPos())
                || !Objects.requireNonNull(P.blockEntity.getLevel()).isLoaded(P.blockEntity.getBlockPos()));


        PERIPHERALS_LIST.add(this);
    }

    public static void messageEvent(String message, String userName, String playerUUID) {
        /*Map<String, Object> data = new HashMap<String, Object>();
        data.put("userName", userName);
        data.put("playerUUID",playerUUID);
        data.put("message", message);*/
        //PERIPHERALS_LIST.removeIf(P -> !P.blockEntity.getLevel().isLoaded(P.blockEntity.getBlockPos()));
        for (ChatBlockPeripheral pc : PERIPHERALS_LIST)
            pc.sendEvent("chat_message", userName, playerUUID, message);

        //CCChatBox.LOGGER.info(PERIPHERALS_LIST.size() + " ");
    }

    private boolean sendSystemMessage(ServerPlayer player, String msg){
        if(player==null) return false;
        player.sendSystemMessage(Component.literal(msg));
        return true;
    }

    private boolean sendMessage(String fakePlayer, ServerPlayer player, String msg){
        if(player==null) return false;

        FakePlayer fPlayer = FakePlayer.get(CCChatBox.INSTANCE.getMinecraftServer().overworld(), new GameProfile(UUID.randomUUID(), fakePlayer));
        ChatType.Bound bind = ChatType.bind(ChatType.CHAT, fPlayer);

        player.sendChatMessage(
                new OutgoingChatMessage.Disguised(Component.literal(msg)),
                false,
                bind
        );

        return true;
    }

    /**
     * send system chat message to all player
     * @param msg The string to be written.
     */
    @LuaFunction
    public final void sendAllSystemMessage(String msg){
        CCChatBox.INSTANCE.getMinecraftServer().getPlayerList().getPlayers().forEach(p->p.sendSystemMessage(Component.literal(msg)));
    }


    /**
     * send system chat message to specific player by userName
     * @param userName player nick.
     * @param msg The string to be written.
     * @return true if found player
     */
    @LuaFunction
    public final boolean sendSystemMessageByName(String userName, String msg){
        ServerPlayer player = CCChatBox.INSTANCE.getMinecraftServer().getPlayerList().getPlayerByName(userName);
        return this.sendSystemMessage(player, msg);
    }


    /**
     * send system chat message to specific player by UUID
     * @param playerUUID player uuid.
     * @param msg The string to be written.
     * @return true if found player
     */
    @LuaFunction
    public final boolean sendSystemMessageByUUID(String playerUUID, String msg){
        ServerPlayer player = CCChatBox.INSTANCE.getMinecraftServer().getPlayerList().getPlayer(UUID.fromString(playerUUID));
        return this.sendSystemMessage(player, msg);
    }

    /**
     * send chat message to all player from fake player
     * @param fakePlayer fake player nick.
     * @param msg The string to be written.
     */
    @LuaFunction
    public final void sendAllMessage(String fakePlayer, String msg){
        FakePlayer player = FakePlayer.get(CCChatBox.INSTANCE.getMinecraftServer().overworld(), new GameProfile(UUID.randomUUID(), fakePlayer));
        ChatType.Bound bind = ChatType.bind(ChatType.CHAT, player);
        OutgoingChatMessage.Disguised disguised = new OutgoingChatMessage.Disguised(Component.literal(msg));

        CCChatBox.INSTANCE.getMinecraftServer().getPlayerList().getPlayers().forEach(p-> p.sendChatMessage(
            disguised,
            false,
            bind
        ));
    }

    /**
     * send chat message to all player by userName from fake player
     * @param fakePlayer fake player nick.
     * @param userName player nick.
     * @param msg The string to be written.
     * @return true if found player.
     */
    @LuaFunction
    public final boolean sendMessageByName(String fakePlayer, String userName, String msg){
        ServerPlayer player = CCChatBox.INSTANCE.getMinecraftServer().getPlayerList().getPlayerByName(userName);
        return this.sendMessage(fakePlayer, player, msg);
    }

    /**
     * send chat message to all player by playerUUID from fake player
     * @param fakePlayer fake player nick.
     * @param playerUUID player uuid.
     * @param msg The string to be written.
     * @return true if found player.
     */
    @LuaFunction
    public final boolean sendMessageByUUID(String fakePlayer, String playerUUID, String msg){
        ServerPlayer player = CCChatBox.INSTANCE.getMinecraftServer().getPlayerList().getPlayer(UUID.fromString(playerUUID));
        return this.sendMessage(fakePlayer, player, msg);
    }
}